import React from 'react'
import classes from '../../../styles/components/WeatherBlock/DateHeader/DateHeader.module.css'

const dateHeader = (props) => {
  return (
    <div className={classes.DateHeader}>
      <h1>{props.date}</h1>
      <h2>{props.location}</h2>
      <h3>Weather for the next 24 hours</h3>
    </div>
  )
}

export default dateHeader