import React from 'react'

const humidity = (props) => {
  return (
    <p>Humidity: {props.humidity}%</p>
  )
}

export default humidity