import React from 'react'

const weatherTemp = (props) => {
  return (
    <div>
      <p>{props.weather}, {props.temp}°C</p>
    </div>
  )
}

export default weatherTemp