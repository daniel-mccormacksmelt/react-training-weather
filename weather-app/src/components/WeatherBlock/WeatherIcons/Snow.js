import React from 'react'

const snow = (props) => {
  return (
    <i className="fas fa-snowflake fa-4x"></i>
  )
}

export default snow