import React from 'react'

const drizzle = (props) => {
  return (
    <i className="fas fa-cloud-shows-heavy fa-4x"></i>
  )
}

export default drizzle