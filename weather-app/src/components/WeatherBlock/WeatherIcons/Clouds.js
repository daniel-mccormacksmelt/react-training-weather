import React from 'react'

const clouds = (props) => {
  return (
    <i className="fas fa-cloud fa-4x"></i>
  )
}

export default clouds