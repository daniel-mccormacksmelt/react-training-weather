import React from 'react'

const thunder = (props) => {
  return (
    <i className="fas fa-bolt fa-4x"></i>
  )
}

export default thunder