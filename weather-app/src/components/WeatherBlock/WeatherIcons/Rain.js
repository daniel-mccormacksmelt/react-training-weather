import React from 'react'

const rain = (props) => {
  return (
    <i className="fas fa-cloud-rain fa-4x"></i>
  )
}

export default rain