import React from 'react'

const wind = (props) => {
  return (
    <p>Wind speed: {props.wind} metres per second</p>
  )
}

export default wind