import React from 'react'
import Time from './Time/Time'
import WeatherTemp from './WeatherTemp/WeatherTemp'
import Humidity from './Humidity/Humidity'
import Wind from './Wind/Wind'
import Atmosphere from './WeatherIcons/Atmosphere'
import Clear from './WeatherIcons/Clear'
import Clouds from './WeatherIcons/Clouds'
import Drizzle from './WeatherIcons/Drizzle'
import Rain from './WeatherIcons/Rain'
import Snow from './WeatherIcons/Snow'
import Thunder from './WeatherIcons/Thunder'

import classes from '../../styles/components/WeatherBlock/WeatherBlock.module.css'

const weatherBlock = (props) => {
  let icon = null;
  switch (props.weather) {
    case('Atmosphere'):
      icon = (<Atmosphere />)
      break;
    case('Clear'):
      icon = (<Clear />)
      break;
    case('Clouds'):
      icon = (<Clouds />)
      break;
    case('Drizzle'):
      icon = (<Drizzle />)
      break;
    case('Rain'):
      icon = (<Rain />)
      break;
    case('Snow'):
      icon = (<Snow />)
      break;
    case('Thunder'):
      icon = (<Thunder />)
      break;
    default:
      break;
  }

  return (
    <div className={classes.WeatherBlock}>
      <div>
        {icon}
        <Time time={props.time} />
        <WeatherTemp weather={props.weather} temp={props.temp} />
      </div>
      <div>
        <Humidity humidity={props.humidity} />
        <Wind wind={props.wind} />
      </div>
    </div>
  )
}

export default weatherBlock