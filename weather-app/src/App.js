import React, { Component } from 'react';
import './App.module.css';
import WeatherBlock from './components/WeatherBlock/WeatherBlock'
import date from 'dateformat'
import DateHeader from './components/WeatherBlock/DateHeader/DateHeader'


import axios from 'axios'

class App extends Component {
  state = {
    weather: [],
    location: ''
  }

  componentDidMount() {
    axios.get('http://api.openweathermap.org/data/2.5/forecast?q=Uppingham,uk&mode=json&appid=760c0135bfb21485ddbf2165ad8ea2e8')
      .then((response) => {
        const nextState = this.state.weather.slice()
        const data = response.data.list.slice(0, 8);
        const location = response.data.city.name

        for(let i = 0; i < data.length; i++){
          const regExp = /\d\d:\d\d/
          const time = data[i].dt_txt.split(' ')[1].toString().match(regExp)
          const weather = {
            time: time,
            weather: data[i].weather[0].main,
            temp: Math.round(data[i].main.temp - 273.15),
            wind: data[i].wind.speed,
            humidity: data[i].main.humidity
          }

          nextState.push(weather)
        }

        this.setState({weather: nextState, location: location})
        console.log(this.state)
      })
  }

  render() {
    const weatherBlocks = this.state.weather.map((block, index) => {
      return <WeatherBlock
              time={block.time}
              weather={block.weather}
              temp={block.temp}
              wind={block.wind}
              humidity={block.humidity}
              key={index} />
    })

    const today = new Date()
    const todayFormatted = date(today, 'dddd, mmmm dS')

    return (
      <div className="App">
        <DateHeader date={todayFormatted} location={this.state.location} />
        {weatherBlocks}
      </div>
    );
  }
}

export default App;
